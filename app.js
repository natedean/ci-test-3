var express = require('express');
var app = express();

console.log('I AM WORKING');

// set the view engine to ejs
app.set('view engine', 'ejs');

// index page
app.get('/', function(req, res) {
    res.render('index');
});

app.listen(8081);
console.log('Server started on port 8081');

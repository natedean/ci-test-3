# ci-test-3

## This repo is intended to test Bitbucket Integration with AWS Elastic Beanstalk

Check the [Pipelines](https://bitbucket.org/natedean/ci-test-3/addon/pipelines/home#!/) section for build/deploy information.

## Checkout the deployments

[master branch, prod environment](http://ci-test-3-prod.wgm72vwaxy.us-west-2.elasticbeanstalk.com/).

[develop branch, dev environment](http://ci-test-3-dev.wgm72vwaxy.us-west-2.elasticbeanstalk.com/).

## These steps are followed to get set up:
* Ensure a bitbucket role is set up in AWS IAM. The bitbucket 'user' should have full deployment priveleges.
* Ensure Application and Environment(s) are set up in AWS Elastic Beanstalk
* Enable pipelines for the repository.
* Set up environment variables in Settings > Pipelines (exact variables listed below in the Required Environment Variables section). 
* Ensure `bitbucket-pipelines.yml` is in the repo's root directory and configured correctly for the repo's needs.
* Ensure the `beanstalk_deploy.py` file, borrowed from the AWS team, is in the repo's root directory.

## Required Environment Variables
* `AWS_SECRET_ACCESS_KEY`:  Secret key for the bitbucket 'user'.
* `AWS_ACCESS_KEY_ID`:  Access key for the bitbucket 'user'.
* `AWS_DEFAULT_REGION`:  Region where the target Elastic Beanstalk application is.
* `APPLICATION_NAME`:  Name of the target Elastic Beanstalk application.
* `APPLICATION_ENVIRONMENT`:  Name of the Elastic Beanstalk environment you are deploying to.
* `S3_BUCKET`:  Name of the S3 Bucket where application versions for the Elastic Beanstalk application are stored.
    * AWS Elastic Beanstalk will create a bucket in the region automatically with a name like `elasticbeanstalk-us-west-2-YOUR_ACCOUNT_NUMBER`.  You can use this bucket to upload your application source.
